#include "MyButton.h"
#include "res.h"

MyButton::MyButton():
	_factorScaleClick(1.1f),
	_enabled(true),
	_hide(false)
{
	setTouchChildrenEnabled(false);
	setText("");
	
    addEventListener(TouchEvent::TOUCH_DOWN, CLOSURE(this, &MyButton::onEvent));
    addEventListener(TouchEvent::TOUCH_UP, CLOSURE(this, &MyButton::onEvent));
    addEventListener(TouchEvent::CLICK, CLOSURE(this, &MyButton::onEvent));
}

void MyButton::onEvent(Event* ev)
{
    TouchEvent* event = static_cast<TouchEvent*>(ev);

	if (ev->type == TouchEvent::TOUCH_DOWN) {
		setColor(color::BTN_DOWN);
	}

	if (ev->type == TouchEvent::TOUCH_UP) {
		setColor(color::BTN_UP);
	}

    if (ev->type == TouchEvent::CLICK){
		//setTouchEnabled(false, false);
		if (isEnable() != false) {
			TyGame::playSound("click");
			auto tween = addTween(Actor::TweenScale(_factorScaleClick), 300, 1, true);
			tween->setDoneCallback([&](Event* ev) {
				//	setTouchEnabled(true, true);
				});
		}
    }
}

void MyButton::setText(const string& txt)
{
    if (!_text){
        TextStyle style;
        style.font = TyGame::gameResources.getResFont("normal");
		style.vAlign = TextStyle::VALIGN_BOTTOM;
        style.hAlign = TextStyle::HALIGN_MIDDLE;

        _text = new TextField;
        _text->setStyle(style);
		_text->attachTo(this);
    }

	_text->setSize(getSize());
	_text->setY(20);

    _text->setText(txt);
}

void MyButton::setFactorScaleClick(float value)
{
	_factorScaleClick = value;
}

void MyButton::setEnable(bool enabled)
{
	_enabled = enabled;

	if (_enabled) {
		setColor(color::BTN_ENABLE);
		_text->setColor(color::TEXT_ENABLE);

		setTouchEnabled(true, true);
	}
	else{
		setColor(color::BTN_DISABLE);
		_text->setColor(color::TEXT_DISABLE);

		setTouchEnabled(false, false);
	}
}

void MyButton::hide(bool hide)
{
	_hide = hide;

	setVisible(!_hide);
}
