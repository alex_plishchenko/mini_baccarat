#include "PlayingCard.h"



PlayingCard::PlayingCard()
{
}


PlayingCard::~PlayingCard()
{
}

void PlayingCard::SetSprite(Sprite* sp)
{
	_sprite = sp;
}

Sprite * PlayingCard::GetSprite()
{
	return _sprite.get();
}

void PlayingCard::SetWeight(std::uint8_t value)
{
	_weight = value;
}

std::uint8_t PlayingCard::GetWeight()
{
	return _weight;
}
