#include <algorithm>
#include <random>

#include "StackPlayingCard.h"

StackPlayingCard::StackPlayingCard() :
	_index_take_card(0),
	_max_size(0)
{

}


StackPlayingCard::~StackPlayingCard()
{
}

void StackPlayingCard::add(PlayingCard* pc)
{
	_v_card.push_back(pc);
}

void StackPlayingCard::setMaxSize(std::size_t value)
{
	_max_size = value;
}

void StackPlayingCard::shuffle()
{
	auto rng = std::default_random_engine{};
	std::shuffle(std::begin(_v_card), std::end(_v_card), rng);

	_index_take_card = 0;
}

PlayingCard* StackPlayingCard::takeCard()
{
	if (_index_take_card < _max_size) {
		PlayingCard* pc = _v_card.at(_index_take_card);
		++_index_take_card;
		return pc;
	}
	return nullptr;
}

bool StackPlayingCard::isHasCard()
{
	return (_index_take_card < _max_size);
}
