#pragma once
#include "oxygine-framework.h"
using namespace oxygine;
using namespace std;

DECLARE_SMART(MyButton, spMyButton);

class MyButton: public Sprite
{
public:
    MyButton();
	virtual ~MyButton() {};

    void setText(const string& txt);
	void setFactorScaleClick(float);
	void setEnable(bool enabled);
	void hide(bool hide);

	bool isEnable() { return _enabled; };
	bool isHide() { return _hide; }

	enum color
	{
		BTN_DOWN = Color::Red,
		BTN_UP = Color::White,
		BTN_DISABLE = Color::Gray,
		BTN_ENABLE = Color::White,

		TEXT_DISABLE = Color::Gray,
		TEXT_ENABLE = Color::White,
	};

private:
    void onEvent(Event*);

	spTextField _text;
	float _factorScaleClick;
	bool _enabled;
	bool _hide;
};