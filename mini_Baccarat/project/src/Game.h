#pragma once
#include "oxygine-framework.h"
#include "Chips.h"
#include "ManageChips.h"
#include "ManageString.h"
#include "PlayingCard.h"
#include "MyButton.h"
#include "BetField.h"
#include "StackPlayingCard.h"
#include "PlayerBase.h"

using namespace oxygine;

DECLARE_SMART(Game, spGame);

class Game: public Actor
{
public:
    Game();
	virtual ~Game();

    void init();

private:
	void createUI();
	void createBackground();
	void createStackPlayingCard();
	void createChips();
	void createButtons();
	void createRateField();

	void upDateInfoPlayer();

	void setBalenceValue(double value);
	void setBetSumaValue(double value);
	void addToBetSumaValue(double value);
	void setActualWinValue(double value);

	void take2Cards();

	void takeCardForPlayer();
	void takeCardForPlayer(std::function<void()> fun_CallBack);
	void takeCardForBank();
	void takeCardForBank(std::function<void()> fun_CallBack);

	void ifCheckWin();

	void winPlayer();
	void winBank();
	void winTie();

	void upDateUiAfterWin();

	void needShufflePlayingCard();
	void btn_continue();

	void on_btn_rate_bank(Event* evn);
	void on_btn_rate_player(Event* evn);
	void on_btn_rate_tie(Event* evn);

	void on_btn_btn_deal(Event* evn);
	void on_btn_btn_continue(Event* evn);
	void on_btn_clear_all(Event* evn);

	void flayCard(PlayingCard* ps, Vector2 &pos, std::function<void()> fun, short priority);
	void awayCardsFromTable();

	Vector2 _target_pos_flay_card_player = { 100, 300 };
	Vector2 _target_pos_flay_bank = { 1024 - 200, 300 };

	std::vector<PlayingCard*> _cards_on_table;

	BankGambler   _bankGambler;
	PlayerGambler _playerGambler;
	std::vector<GamblerBase*> _list_gamblers;

	StackPlayingCard _stackPlayingCard;
	ManageChips _manageChips;

	spActor _ui;
	spTextField _text_score_player;
	spTextField _text_score_bank;

	spTextField _text_balence;
	spTextField _text_suma_bet;
	spTextField _text_suma_win;
	spTextField _text_name_win;

	spBetField _btn_bet_field_bank;
	spBetField _btn_bet_field_player;
	spBetField _btn_bet_field_tie;

	spMyButton _btn_deal;
	spMyButton _btn_continue;
	spMyButton _btn_clear_all;

	int _flay_duration;
	double _suma_bet;
};