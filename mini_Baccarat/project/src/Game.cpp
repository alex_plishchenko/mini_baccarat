#include "Game.h"
#include "res.h"
#include <iomanip>
#include <sstream>

#include "oxygine/json/json.h"

#define MgString ManageString::getInstance()

static const double BALENCE_PLAYER = 10000.0;

Game::Game():
	_suma_bet(0.0),
	_flay_duration(400)
{

	_list_gamblers.push_back(&_bankGambler);
	_list_gamblers.push_back(&_playerGambler);
}

Game::~Game()
{

}

void Game::init()
{
	MgString.Load("Language.json");

    //scene layer would have size of display
    setSize(getStage()->getSize());
	    
	_ui = new Actor;
    _ui->attachTo(this);
    //it would be higher than other actors with default priority = 0
    _ui->setPriority(1);
	//_ui->setSize(getStage()->getSize());

	createUI();
	createChips();
	createStackPlayingCard();

	upDateInfoPlayer();
}

void Game::createUI()
{
	createBackground();
	createButtons();
	createRateField();

	TextStyle style;
	style.font = TyGame::gameResources.getResFont("normal");
	style.vAlign = TextStyle::VALIGN_MIDDLE;
	style.hAlign = TextStyle::HALIGN_MIDDLE;

	_text_score_player = new TextField;
	_text_score_player->setStyle(style);
	_text_score_player->setColor(Color::White);
	_text_score_player->setText("");
	_text_score_player->attachTo(this);
	_text_score_player->setPosition(_target_pos_flay_card_player.x, _target_pos_flay_card_player.y + 100);

	_text_score_bank = new TextField;
	_text_score_bank->setStyle(style);
	_text_score_bank->setColor(Color::White);
	_text_score_bank->setText("");
	_text_score_bank->attachTo(this);
	_text_score_bank->setPosition(_target_pos_flay_bank.x, _target_pos_flay_bank.y + 100);

	// text win

	_text_name_win = new TextField;
	_text_name_win->setStyle(style);
	_text_name_win->setColor(Color::White);
	_text_name_win->setText("");
	_text_name_win->setAnchor(0.0f, 0.5f);
	_text_name_win->attachTo(this);
	_text_name_win->setPosition(getWidth()/2, 100);

	{
		// Create Text panel_down;
		const float offset_y = 700;
		TextStyle style;
		style.font = TyGame::gameResources.getResFont("normal");
		style.vAlign = TextStyle::VALIGN_BOTTOM;
		style.hAlign = TextStyle::HALIGN_LEFT;

		_text_balence = new TextField;
		_text_balence->setAnchor(0.0f, 0.5f);
		_text_balence->setScale(0.5f);
		_text_balence->setStyle(style);
		_text_balence->setColor(Color::White);
		_text_balence->setText(MgString.GetStringFromKey("text_balence"));
		_text_balence->attachTo(this);
		_text_balence->setPosition(getStage()->getSize().x * 0.1f, offset_y);

		_text_suma_bet = new TextField;
		_text_suma_bet->setScale(0.5f);
		_text_suma_bet->setStyle(style);
		_text_suma_bet->setColor(Color::White);
		_text_suma_bet->setText(MgString.GetStringFromKey("text_bet"));
		_text_suma_bet->attachTo(this);
		_text_suma_bet->setPosition(getStage()->getSize().x * 0.4f, offset_y);

		_text_suma_win = new TextField;
		_text_suma_win->setScale(0.5f);
		_text_suma_win->setStyle(style);
		_text_suma_win->setColor(Color::White);
		_text_suma_win->setText(MgString.GetStringFromKey("text_win"));
		_text_suma_win->attachTo(this);
		_text_suma_win->setPosition(getStage()->getSize().x * 0.7f, offset_y);
	}
}

void Game::createBackground()
{
	spSprite background = new Sprite;
	background->setResAnim(TyGame::gameResources.getResAnim("images/Background"));
	background->attachTo(this);
	background->setSize(getStage()->getSize());
}

void Game::createStackPlayingCard()
{
	const ResAnim* cards = TyGame::gameResources.getResAnim("cards");
	const int n_card = 13;

	std::uint8_t index_weight_card[n_card] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 0, 0, 0 };

	const int n_cards = cards->getRows() * cards->getColumns() - 1;

	_stackPlayingCard.setMaxSize(n_cards - 1);

	for (size_t i = 0, cur_index_card = 0; i < n_cards; i++)
	{
		const AnimationFrame& card_frame = cards->getFrame(i);
		auto card = new Sprite;

		card->setAnchor(0.5, 0.5);
		card->setAnimFrame(card_frame);
		addChild(card);
		card->setVisible(false);

		auto pCard = new PlayingCard;
		pCard->SetSprite(card);

		pCard->SetWeight(index_weight_card[cur_index_card]);

		_stackPlayingCard.add(pCard);

		cur_index_card++;
		if (cur_index_card != 0 && cur_index_card % n_card == 0) {
			cur_index_card = 0;
		}
	}

	_stackPlayingCard.shuffle();
}

void Game::createChips()
{
	const std::size_t n_chip = 4;
	std::uint8_t chip_values[n_chip] = { 1, 5, 25, 100 };

	const float dx = 680;
	const float dy = 625;

	const float x_step = 10;
	float chip_w = 0;
	float chip_h = 0;
	float pos_last_chip_x = 0;
	const ResAnim* btns_chips = TyGame::gameResources.getResAnim("chips");

	for (size_t i = 0; i < btns_chips->getColumns()-1; i++)
	{
		const AnimationFrame& frame = btns_chips->getFrame(i, 0);
		const AnimationFrame& glow_frame = btns_chips->getFrame(4, 0);

		float scale = btns_chips->getAttribute("scale").as_float(1.0);

		spChips chip = new Chips();
		chip->setAnimFrame(frame);
		chip->setScale(scale);
		chip->setAnchor(0.5, 0.5);

		spSprite glow = new Sprite;
		glow->setAnimFrame(glow_frame);

		chip->SetGlow(glow);

		chip_w = chip->getScaledWidth();
		chip_h = chip->getScaledHeight();
		float x = dx + i * chip_w + i * x_step;
		pos_last_chip_x = x;

		chip->setPosition(x, dy);

		chip->SetValue(chip_values[i]);

		chip->initUi();

		chip->attachTo(this);
		chip->setPriority(1);

		_manageChips.AddChip(chip.get());
	}

	{
		Vector2 pos = { dx - chip_w / 2, dy - chip_h / 2 };
		auto plate_chip = new ColorRectSprite;
		plate_chip->setPosition(pos.x, pos.y);

		plate_chip->setSize(Vector2(pos_last_chip_x - pos.x + chip_w / 2, chip_h));
		plate_chip->setColor(Color(32, 32, 32, 200));
		plate_chip->attachTo(this);
	}
}

void Game::createButtons()
{
	const ResAnim* btns = TyGame::gameResources.getResAnim("buttons");

	_btn_deal = new MyButton;
	_btn_continue = new MyButton;
	_btn_clear_all = new MyButton;

	_btn_deal->setAnchor(0.5f, 0.5f);
	_btn_continue->setAnchor(0.5f, 0.5f);
	_btn_clear_all->setAnchor(0.5f, 0.5f);

	_btn_deal->setAnimFrame(btns->getFrame(1));
	_btn_continue->setAnimFrame(btns->getFrame(2));
	_btn_clear_all->setAnimFrame(btns->getFrame(0));
	
	_btn_deal->attachTo(this);
	_btn_continue->attachTo(this);
	_btn_clear_all->attachTo(this);

	const float btn_scale = 0.6f;
	const float btn_y = 625;

	_btn_deal->setFactorScaleClick(btn_scale * 1.1);
	_btn_continue->setFactorScaleClick(btn_scale * 1.1);
	_btn_clear_all->setFactorScaleClick(btn_scale * 1.1);

	
	_btn_deal->setScale(btn_scale);
	_btn_continue->setScale(btn_scale);
	_btn_clear_all->setScale(btn_scale);

	_btn_deal->setPosition(512 - 96 / 2, btn_y);
	_btn_continue->setPosition(getStage()->getSize().x / 2, btn_y);
	_btn_clear_all->setPosition(512 + 96 / 2, btn_y);
	
	_btn_deal->setName("btn_deal");
	_btn_continue->setName("btn_reest");
	_btn_clear_all->setName("btn_clear_all");

	_btn_deal->setText(MgString.GetStringFromKey("text_btn_deal"));
	_btn_continue->setText(MgString.GetStringFromKey("text_btn_continue"));
	_btn_clear_all->setText(MgString.GetStringFromKey("text_btn_clear_all"));

	_btn_continue->hide(true);
	
	_btn_deal->setEnable(false);
	_btn_clear_all->setEnable(false);
	
	_btn_deal->addEventListener(TouchEvent::CLICK, CLOSURE(this, &Game::on_btn_btn_deal));
	_btn_continue->addEventListener(TouchEvent::CLICK, CLOSURE(this, &Game::on_btn_btn_continue));
	_btn_clear_all->addEventListener(TouchEvent::CLICK, CLOSURE(this, &Game::on_btn_clear_all));
}

void Game::createRateField()
{
	float y_start = 200;
	const float y_step = 10;
	const Vector2 size = Vector2(400, 80);

	_btn_bet_field_bank = new BetField;
	_btn_bet_field_player = new BetField;
	_btn_bet_field_tie = new BetField;

	_btn_bet_field_bank->attachTo(this);
	_btn_bet_field_player->attachTo(this);
	_btn_bet_field_tie->attachTo(this);

	_btn_bet_field_bank->create("bank", size, Vector2(getStage()->getWidth() / 2 - size.x / 2, y_start));
	_btn_bet_field_bank->setTextName(MgString.GetStringFromKey("text_rate_bank"));

	y_start += y_step + size.y;

	_btn_bet_field_player->create("player", size, Vector2(getStage()->getWidth() / 2 - size.x / 2, y_start));
	_btn_bet_field_player->setTextName(MgString.GetStringFromKey("text_rate_player"));

	y_start += y_step + size.y;

	_btn_bet_field_tie->create("tie", size, Vector2(getStage()->getWidth() / 2 - size.x / 2, y_start));
	_btn_bet_field_tie->setTextName(MgString.GetStringFromKey("text_rate_tie"));

	_btn_bet_field_bank->addEventListener(TouchEvent::CLICK, CLOSURE(this, &Game::on_btn_rate_bank));
	_btn_bet_field_player->addEventListener(TouchEvent::CLICK, CLOSURE(this, &Game::on_btn_rate_player));
	_btn_bet_field_tie->addEventListener(TouchEvent::CLICK, CLOSURE(this, &Game::on_btn_rate_tie));
}

void Game::upDateInfoPlayer()
{
	_playerGambler.setBalence(BALENCE_PLAYER);

	setBalenceValue(_playerGambler.getBalence());
	setActualWinValue(0);
	setBetSumaValue(0);
}

void Game::setBalenceValue(double value)
{
	std::string str = MgString.GetStringFromKey("text_balence");
	string string_value;
	string_value.resize(10);

	sprintf(const_cast<char*>(string_value.data()), " %.2f", value);
	str += string_value;

	_text_balence->setText(str);
}

void Game::setBetSumaValue(double value)
{
	std::string str = MgString.GetStringFromKey("text_bet");
	string string_value;
	string_value.resize(10);

	sprintf(const_cast<char*>(string_value.data()), " %.2f $", value);
	str += string_value;

	_text_suma_bet->setText(str);
}

void Game::addToBetSumaValue(double value)
{
	_suma_bet += value;
	setBetSumaValue(_suma_bet);
}

void Game::setActualWinValue(double value)
{
	std::string str = MgString.GetStringFromKey("text_win");
	string string_value;
	string_value.resize(10);

	sprintf(const_cast<char*>(string_value.data()), " %.2f $", value);
	str += string_value;

	_text_suma_win->setText(str);
}

void Game::take2Cards()
{
	std::function<void()> take_card;

	take_card = [&](){
		auto callBack_bank = [&]() {
			const int score_player = _playerGambler.getScoreCard();
			const int score_bank = _bankGambler.getScoreCard();

			const int n_card_in_bank = _bankGambler.getNHaveCard();
			if (n_card_in_bank == 1) {
				take2Cards();
			}
			else {
				if (_playerGambler.getNHaveCard() == 2 && (score_player >= 0 && score_player <= 5)) {
					takeCardForPlayer();
				}
				
				if (_bankGambler.getNHaveCard() == 2 && (score_bank >= 0 && score_bank <= 4)) {
					takeCardForBank();
				}
				
				ifCheckWin();
			}
		};

		auto callBack_card_1 = [&, callBack_bank]() {
			takeCardForBank(callBack_bank);
		};

		takeCardForPlayer(callBack_card_1);
	};

	take_card();
}

void Game::takeCardForPlayer()
{
	takeCardForPlayer(nullptr);
}

void Game::takeCardForPlayer(std::function<void()> fun_CallBack)
{
	if (_stackPlayingCard.isHasCard()) {
		PlayingCard* card_playing = _stackPlayingCard.takeCard();
		const int score_card_p = card_playing->GetWeight();
		const int score = _playerGambler.addWeightCard(card_playing->GetWeight());

		auto fun_callBack = [&, score, fun_CallBack]() {
			_text_score_player->setText(std::to_string(score));
			if (fun_CallBack) {
				fun_CallBack();
			}
			//ifCheckWin();
		};

		Vector2 target_pos = _target_pos_flay_card_player;
		const int n_card = _playerGambler.getNHaveCard();
		const float w_card = card_playing->GetSprite()->getScaledWidth();

		if (n_card > 1) {
			target_pos.x += w_card * 0.3f * (n_card - 1);
		}

		const int priority = _playerGambler.getNHaveCard() + 1;
		_cards_on_table.push_back(card_playing);

		flayCard(card_playing, target_pos, fun_callBack, priority);
	}else{
		needShufflePlayingCard();
	}
}

void Game::ifCheckWin()
{
	const int score_player = _playerGambler.getScoreCard();
	const int score_bank = _bankGambler.getScoreCard();
	const int n_card_player = _playerGambler.getNHaveCard();
	const int n_card_bank = _bankGambler.getNHaveCard();

	if ((n_card_player == 3) && (n_card_bank == 3)){
		if (score_bank > score_player) {
			winBank();
		}
		else if (score_bank < score_player) {
			winPlayer();
		}
		else{
			winTie();
		}
	}

	if ((score_player == score_bank) || (score_player == 9 && score_bank == 9)) {
		winTie();
	}
	else if ((score_player == 9) || (score_bank == 9)) {
		if (score_player == 9) {
			winPlayer();
		}
		else {
			winBank();
		}
	}
	else if ((score_player == 8) || (score_bank == 8)) {
		if (score_bank < 8) {
			winPlayer();
		}
		else {
			winBank();
		}
	}
}

void Game::winPlayer()
{
	upDateUiAfterWin();

	_text_name_win->setText(MgString.GetStringFromKey("text_win_player"));

	const int bet = static_cast<int>(_playerGambler.getBetPlayer());

	if (bet > 0) {
		const double paid = bet;
		const double win_total = bet + paid;
		const double balence = _playerGambler.getBalence() + win_total;
		_playerGambler.setBalence(balence);
		setBalenceValue(balence);

		setActualWinValue(paid);
	}
}

void Game::winBank()
{
	upDateUiAfterWin();
	
	_text_name_win->setText(MgString.GetStringFromKey("text_win_bank"));

	const int bet = static_cast<int>(_playerGambler.getBetBank());

	if (bet > 0) {
		const double paid = bet - bet * 0.05;
		const double win_total = bet + paid;
		const double balence = _playerGambler.getBalence() + win_total;
		_playerGambler.setBalence(balence);
		setBalenceValue(balence);

		setActualWinValue(paid);
	}
}

void Game::winTie()
{
	upDateUiAfterWin();

	_text_name_win->setText(MgString.GetStringFromKey("text_win_tie"));

	const int bet = static_cast<int>(_playerGambler.getBetTie());

	if (bet > 0) {
		const double paid = bet * 9 - bet;
		const double win_total = bet * 9;
		const double balence = _playerGambler.getBalence() + win_total;
		_playerGambler.setBalence(balence);
		setBalenceValue(balence);

		setActualWinValue(paid);
	}
}

void Game::upDateUiAfterWin()
{
	TyGame::playSound("win");
	_btn_continue->setEnable(true);
}

void Game::needShufflePlayingCard()
{
	_text_name_win->setText(MgString.GetStringFromKey("text_need_shuffle_playing_card"));

	_btn_continue->setEnable(true);

	_stackPlayingCard.shuffle();
}

void Game::btn_continue()
{
	awayCardsFromTable();

	_btn_deal->hide(false);
	_btn_clear_all->hide(false);
	_btn_continue->hide(true);

	_btn_bet_field_bank->setTouchEnabled(true, true);
	_btn_bet_field_player->setTouchEnabled(true, true);
	_btn_bet_field_tie->setTouchEnabled(true, true);

	_text_score_player->setText("");
	_text_score_bank->setText("");
	_text_name_win->setText("");

	setActualWinValue(0);

	on_btn_clear_all(nullptr);
}

void Game::takeCardForBank()
{
	takeCardForBank(nullptr);
}

void Game::takeCardForBank(std::function<void()> fun_CallBack)
{
	if (_stackPlayingCard.isHasCard()) {
		PlayingCard* card_bank = _stackPlayingCard.takeCard();
		const int score = _bankGambler.addWeightCard(card_bank->GetWeight());

		auto fun_callBack = [&, score, fun_CallBack]() {
			_text_score_bank->setText(std::to_string(score));
			if (fun_CallBack) {
				fun_CallBack();
			}
			//ifCheckWin();
		};

		Vector2 target_pos = _target_pos_flay_bank;
		const int n_card = _bankGambler.getNHaveCard();
		const float w_card = card_bank->GetSprite()->getScaledWidth();

		if (n_card > 1) {
			target_pos.x += w_card * 0.3f * (n_card - 1);
		}

		const int priority = _bankGambler.getNHaveCard() + 1;
		flayCard(card_bank, target_pos, fun_callBack, priority);

		_cards_on_table.push_back(card_bank);
	}else{
		needShufflePlayingCard();
	}
}

void Game::on_btn_rate_bank(Event * evn)
{
	Chips* coin = _manageChips.GetSelectCoin();

	if (coin != nullptr) {
		TyGame::playSound("coin_use");
		const auto coin_value = coin->GetValue();
		_btn_bet_field_bank->addBetValue(coin_value);

		_playerGambler.addBetBank(coin_value);
		addToBetSumaValue(coin_value);

		_playerGambler.setBalence(_playerGambler.getBalence() - coin_value);
		setBalenceValue(_playerGambler.getBalence());

		_btn_deal->setEnable(true);
		_btn_clear_all->setEnable(true);
	}
}

void Game::on_btn_rate_player(Event * evn)
{
	Chips* coin = _manageChips.GetSelectCoin();

	if (coin != nullptr) {
		TyGame::playSound("coin_use");
		const auto coin_value = coin->GetValue();
		_btn_bet_field_player->addBetValue(coin_value);
		
		_playerGambler.addBetPlayer(coin_value);
		addToBetSumaValue(coin_value);

		_playerGambler.setBalence(_playerGambler.getBalence() - coin_value);
		setBalenceValue(_playerGambler.getBalence());

		_btn_deal->setEnable(true);
		_btn_clear_all->setEnable(true);
	}
}

void Game::on_btn_rate_tie(Event * evn)
{
	Chips* coin = _manageChips.GetSelectCoin();

	if (coin != nullptr) {
		TyGame::playSound("coin_use");
		const auto coin_value = coin->GetValue();
		_btn_bet_field_tie->addBetValue(coin_value);

		_playerGambler.addBetTie(coin_value);
		addToBetSumaValue(coin_value);

		_playerGambler.setBalence(_playerGambler.getBalence() - coin_value);
		setBalenceValue(_playerGambler.getBalence());

		_btn_deal->setEnable(true);
		_btn_clear_all->setEnable(true);
	}
}

void Game::on_btn_btn_deal(Event * evn)
{
	_btn_deal->hide(true);
	_btn_clear_all->hide(true);

	_btn_continue->hide(false);
	_btn_continue->setEnable(false);

	_btn_bet_field_bank->setTouchEnabled(false, false);
	_btn_bet_field_player->setTouchEnabled(false, false);
	_btn_bet_field_tie->setTouchEnabled(false, false);

	take2Cards();
}

void Game::on_btn_btn_continue(Event * evn)
{
	btn_continue();
}

void Game::on_btn_clear_all(Event * evn)
{
	_suma_bet = 0;
	setBetSumaValue(_suma_bet);

	_btn_deal->setEnable(false);
	_btn_clear_all->setEnable(false);

	_btn_bet_field_bank->setBetValue(0);
	_btn_bet_field_player->setBetValue(0);
	_btn_bet_field_tie->setBetValue(0);

	_bankGambler.resetBetAll();
	_playerGambler.resetBetAll();

	_bankGambler.resetScore();
	_playerGambler.resetScore();
}

void Game::flayCard(PlayingCard* ps, Vector2& pos, std::function<void()> fun, short priority)
{
	const int flay_scale = 50;//ms
	const short card_back_priorityZ = 100;

	const ResAnim* cards = TyGame::gameResources.getResAnim("cards");

	const AnimationFrame& card_back_frame = cards->getFrame(16, 2); //card_back

	auto card_back = new Sprite;
	card_back->setAnchor(0.5, 0.5);
	card_back->setAnimFrame(card_back_frame);
	card_back->setPriority(card_back_priorityZ);

	addChild(card_back);
	card_back->setPosition(getStage()->getWidth(), 0);

	auto card = ps->GetSprite();

	card->setScaleX(0);
	card->setVisible(true);
	card->setPriority(priority);

	card->setPosition(pos);

	spTweenQueue faly_tween = new TweenQueue;
	faly_tween->add(Actor::TweenPosition(pos), _flay_duration, true, false, 0, Tween::ease_inOutCubic);
	auto scale_tween = faly_tween->add(Actor::TweenScaleX(0), flay_scale, true, false);

	card_back->addTween(faly_tween);

	scale_tween->addDoneCallback([card, flay_scale, card_back, fun](Event * evn) {
		card->addTween(Actor::TweenScaleX(1), flay_scale);
		fun();
		//removeChild(card_back);
		});
}

void Game::awayCardsFromTable()
{
	Vector2 pos = Vector2(-100, -100);

	for (auto &card: _cards_on_table) {
		card->GetSprite()->addTween(Actor::TweenPosition(pos), _flay_duration, true, false, 0, Tween::ease_inOutCubic);
	}
	_cards_on_table.clear();
}
