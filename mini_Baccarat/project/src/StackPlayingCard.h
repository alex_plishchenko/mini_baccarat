#pragma once

#include "PlayingCard.h"

class StackPlayingCard
{
public:
	StackPlayingCard();
	virtual ~StackPlayingCard();

	void add(PlayingCard*);
	void setMaxSize(std::size_t);

	void shuffle();

	PlayingCard*  takeCard();
	bool isHasCard();

private:
	std::vector<PlayingCard*> _v_card;
	std::size_t _index_take_card;
	std::size_t _max_size;
};

