#include <numeric>
#include "PlayerBase.h"

int GamblerBase::addWeightCard(std::size_t weight_card)
{
	_score.push_back(weight_card);

	return getScoreCard();
}

int GamblerBase::getScoreCard()
{
	int score = std::accumulate(_score.begin(), _score.end(), 0);
	const int factor_dec = 10;

	score = (score >= factor_dec) ? (score - factor_dec) : score;

	return score;
}

int GamblerBase::getNHaveCard()
{
	return _score.size();
}

void GamblerBase::resetScore()
{
	_score.clear();
}

void GamblerBase::resetBetAll()
{
	_bet_bank = 0;
	_bet_player = 0;
	_bet_tie = 0;
}

double GamblerBase::addBetBank(double value)
{
	_bet_bank += value;
	return _bet_bank;
}

double GamblerBase::addBetPlayer(double value)
{
	_bet_player += value;

	return _bet_player;
}

double GamblerBase::addBetTie(double value)
{
	_bet_tie += value;

	return _bet_tie;
}

double GamblerBase::getBetBank()
{
	return _bet_bank;
}

double GamblerBase::getBetPlayer()
{
	return _bet_player;
}

double GamblerBase::getBetTie()
{
	return _bet_tie;
}
