#include "ManageChips.h"

ManageChips::ManageChips():
	_index_select_chip(-1)
{
}


ManageChips::~ManageChips()
{
}

void ManageChips::AddChip(Chips* chip)
{
	_chips.push_back(chip);
	chip->_manageChips = this;

	auto callBack = [&](Event* ev, int i_chip) {
		onEven_CLICK(ev, i_chip);
	};

	const int i_chip = _chips.size();

	using namespace std::placeholders;
	auto event_callBack = std::bind(callBack, _1, i_chip);

	chip->addEventListener(TouchEvent::CLICK, event_callBack);
}

Chips* ManageChips::GetSelectCoin()
{
	if (_index_select_chip != -1) {
		return _chips[_index_select_chip];
	}
	return nullptr;
}

void ManageChips::onEven_CLICK(Event *ev, int index_chip)
{
	TouchEvent* event = static_cast<TouchEvent*>(ev);
	const int i_chip = index_chip - 1;

	bool select = !_chips[i_chip]->IsSelect();

	std::for_each(_chips.begin(), _chips.end(), [](Chips* chip) {
		chip->Select(false);
	});

	_chips[i_chip]->Select(select);

	_index_select_chip = (select == true) ? i_chip : -1;

}