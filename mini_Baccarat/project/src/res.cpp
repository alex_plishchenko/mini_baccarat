#include "res.h"
#include "oxygine-sound.h"
#include <map>

extern  SoundPlayer player;

namespace TyGame {
	
	Resources gameResources;
	typedef std::map<string, ResSound*> Sounds;
	
	Sounds sounds;

	void ResourcesLoad()
	{
		gameResources.loadXML("resources.xml");

		sounds["click"] = ResSound::create("sounds/button_click.ogg", false);
		sounds["coin_use"] = ResSound::create("sounds/coin_use.ogg", false);
		sounds["win"] = ResSound::create("sounds/win.ogg", false);
	}

	void ResourcesFree()
	{
		player.stop();
		gameResources.free();

		for (Sounds::iterator i = sounds.begin(); i != sounds.end(); ++i)
		{
			delete i->second;
		}
	}

	void playSound(const std::string& id)
	{
		player.play(sounds[id]);
	}
}