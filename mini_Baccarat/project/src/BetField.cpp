#include "BetField.h"
#include "res.h"

BetField::BetField():
	_bet_value(0)
{
	setColor(Color(Color::White, 5));

	setTextName("");

	addEventListener(TouchEvent::OVER, CLOSURE(this, &BetField::onEvent));
	addEventListener(TouchEvent::OUTX, CLOSURE(this, &BetField::onEvent));
}


BetField::~BetField()
{
}

void BetField::create(const string & name, const Vector2 & size, const Vector2 & pos)
{
	setName(name);
	setSize(size);
	setPosition(pos);
}

void BetField::setTextName(const string & txt)
{
	if (!_text_name)
	{
		TextStyle style;
		style.font = TyGame::gameResources.getResFont("normal");
		style.vAlign = TextStyle::VALIGN_MIDDLE;
		style.hAlign = TextStyle::HALIGN_MIDDLE;

		_text_name = new TextField;
		_text_name->setStyle(style);
		_text_name->setColor(Color::Gray);
		_text_name->setText("");
		_text_name->attachTo(this);
		_text_name->setPosition(getWidth() / 2, 10);
	}

	_text_name->setPosition(getWidth() / 2, 10);

	_text_name->setText(txt);
}

void BetField::setBetValue(std::uint64_t value)
{
	std::string text;

	_bet_value = value;

	if (!_text_bet)
	{
		TextStyle style;
		style.font = TyGame::gameResources.getResFont("normal");
		style.vAlign = TextStyle::VALIGN_MIDDLE;
		style.hAlign = TextStyle::HALIGN_MIDDLE;

		_text_bet = new TextField;
		_text_bet->setStyle(style);
		_text_bet->setColor(Color::White);
		_text_bet->setText("");
		_text_bet->attachTo(this);
		_text_bet->setPosition(getWidth() / 2, 10);
		_text_bet->setScale(0.7f);
	}

	_text_bet->setPosition(getWidth() / 2, getHeight() * 0.7f);

	if (_bet_value > 0) {
		text = std::to_string(_bet_value) + "$";
	}

	_text_bet->setText(text);
}

void BetField::addBetValue(std::uint64_t value)
{
	setBetValue(_bet_value + value);
}

std::uint64_t BetField::getBetValue()
{
	return _bet_value;
}

void BetField::onEvent(Event* ev)
{
	if (ev->type == TouchEvent::OVER) {
		setColor(Color(Color::White, 20));
	}

	if (ev->type == TouchEvent::OUTX) {
		setColor(Color(Color::White, 5));
	}
}
