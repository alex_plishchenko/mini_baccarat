#pragma once
#include <vector>

class GamblerBase
{
public:
	GamblerBase():
		_balence(0),
		_bet_bank(0),
		_bet_player(0),
		_bet_tie(0)
	{};

	~GamblerBase() {};

	virtual double getBalence() { return _balence; };
	virtual void setBalence(double value) { _balence = value; };

	virtual int addWeightCard(std::size_t);
	virtual int getScoreCard();
	virtual int getNHaveCard();

	virtual void resetScore();
	virtual void resetBetAll();

	virtual double addBetBank(double value);
	virtual double addBetPlayer(double value);
	virtual double addBetTie(double value);

	virtual double getBetBank();
	virtual double getBetPlayer();
	virtual double getBetTie();

private:
	std::vector<std::size_t> _score;
	double _balence;

	double _bet_bank;
	double _bet_player;
	double _bet_tie;
};

class PlayerGambler:public GamblerBase
{
public:
	PlayerGambler() {};
	~PlayerGambler() {};

private:

};

class BankGambler: public GamblerBase
{
public:
	BankGambler() {};
	~BankGambler() {};

private:

};