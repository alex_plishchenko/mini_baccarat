#include "ManageString.h"
#include <fstream> 

ManageString::ManageString()
{
	_prefixNoString = "NO_STRING_";
}

std::string ManageString::GetStringFromKey(const char * key)
{
	return GetStringFromKey(std::string(key));
}

std::string ManageString::GetStringFromKey(const std::string & key)
{
	std::string res;

	if (_root[key].isNull() == false) {
		res = _root[key].asCString();
	}
	else {
		res += _prefixNoString;
		res += key;
	}
	return res;
}

bool ManageString::Load(const char * file_name)
{
	return Load(std::string(file_name));
}

bool ManageString::Load(std::string& file_name)
{
	std::ifstream language_json(file_name);

	if (!language_json.is_open()) { return false; }
	if (!_json.parse(language_json, _root)) { return false; }
}
