#pragma once
#include "oxygine-framework.h"
using namespace oxygine;

class PlayingCard
{
public:
	PlayingCard();
	~PlayingCard();

	void SetSprite(Sprite*);
	Sprite* GetSprite();
	void SetWeight(std::uint8_t);
	std::uint8_t GetWeight();

private:
	spSprite _sprite;
	std::string _name;
	std::uint8_t _weight;
};

