#pragma once
#include "oxygine-framework.h"
#include "ManageChips.h"

using namespace oxygine;

class ManageChips;

DECLARE_SMART(Chips, spChips);

class Chips : public Sprite
{
public:
	explicit Chips();
	virtual ~Chips();

	void SetValue(std::uint8_t value);
	void SetGlow(spSprite& sp);
	void Select(bool flag);
	bool IsSelect() { return _is_select; }
	void initUi();

	std::uint8_t GetValue() { return _value; };

	ManageChips* _manageChips;
private:
	void onEvent(Event*);

	bool _is_select;

	spSprite _glow;
	spTextField _text;
	std::uint8_t _value;
	const std::string _sufix_value = "$";
	float _scale_base;

	static const constexpr float _factor_scale = 1.1f;
	static const int _factor_time = 250;
	static const int _sizeText = 40;
};
