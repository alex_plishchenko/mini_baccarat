#include "oxygine-framework.h"
#include "oxygine-sound.h"
#include "res.h"
#include "Game.h"
#include "GameScene.h"
#include "ResFontFT.h"

using namespace oxygine;


SoundPlayer player;

void example_preinit()
{
}

void example_init()
{
	ResFontFT::initLibrary();
    ResFontFT::setSnapSize(10);

    SoundSystem::create()->init(4);
    
    //load resources
	TyGame::ResourcesLoad();

	flow::init();

    GameScene::instance = new GameScene;
    
    flow::show(GameScene::instance);
}

void example_update()
{
    SoundSystem::get()->update();
    player.update();
	flow::update();
}

void example_destroy()
{
    GameScene::instance = 0;

	TyGame::ResourcesFree();
	ResFontFT::freeLibrary();

    SoundSystem::free();
	flow::free();
}