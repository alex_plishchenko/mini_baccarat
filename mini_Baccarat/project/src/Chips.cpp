#include "Chips.h"
#include "res.h"

Chips::Chips(): _is_select(false)
{
	//_manageChips = manageChips;
	addEventListener(TouchEvent::OVER, CLOSURE(this, &Chips::onEvent));
	addEventListener(TouchEvent::OUTX, CLOSURE(this, &Chips::onEvent));

	_value = 0;

	_text  = new TextField();
	_text->attachTo(this);
}

Chips::~Chips()
{

}

void Chips::SetValue(std::uint8_t value)
{
	_value = value;
	std::string str_value = std::to_string(_value) + _sufix_value;
	_text->setText(str_value);
}

void Chips::SetGlow(spSprite& sp)
{
	_glow = sp;
	addChild(_glow);
	_glow->setVisible(false);
}

void Chips::Select(bool flag)
{
	_glow->setVisible(flag);
	_is_select = flag;
}

void Chips::initUi()
{
	TextStyle style = TextStyle(TyGame::gameResources.getResFont("normal")).withColor(Color::Red).alignMiddle().withFontSize(_sizeText);
	_text->setStyle(style);

	_text->setPosition(getSize() / 2);
	SetValue(_value);
	_scale_base = getScale().x;
}

void Chips::onEvent(Event *ev)
{
	TouchEvent* event = static_cast<TouchEvent*>(ev);

	if (ev->type == TouchEvent::OVER) {
		setPriority(getPriority() + 1);
		addTween(Actor::TweenScale(_scale_base * _factor_scale), _factor_time);
	}

	if (ev->type == TouchEvent::OUTX) {
		setPriority(getPriority() - 1);
		addTween(Actor::TweenScale(_scale_base / _factor_scale), _factor_time);
	}
}
