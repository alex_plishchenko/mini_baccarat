#pragma once
#include "Chips.h"
#include "oxygine-framework.h"

using namespace oxygine;

class Chips;

class ManageChips
{
public:
	ManageChips();
	~ManageChips();

	void AddChip(Chips* chip);

	int GetIndexSelectCoin() { return _index_select_chip; } // error -1;
	Chips* GetSelectCoin(); // error nullptr
	
private:
	void onEven_CLICK(Event *ev, int);

	std::vector<Chips*> _chips;
	int _index_select_chip;
};

