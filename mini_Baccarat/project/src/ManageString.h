#pragma once
#include <string>
#include "oxygine/json/json.h"

class ManageString
{
private:
	ManageString();
	ManageString(const ManageString&) = delete;
	ManageString& operator=(ManageString&) = delete;

public:
	static ManageString& getInstance() {
		static ManageString  instance;
		return instance;
	}

	std::string GetStringFromKey(const char* key);
	std::string GetStringFromKey(const std::string& key);

	bool Load(const char* file_name);
	bool Load(std::string& file_name);

private:
	Json::Reader _json;
	Json::Value _root;

	std::string _prefixNoString;
};

