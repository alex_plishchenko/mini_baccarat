#pragma once
#include "oxygine-framework.h"
using namespace oxygine;
using namespace std;

DECLARE_SMART(BetField, spBetField);

class BetField :public ColorRectSprite
{
public:
	BetField();
	virtual ~BetField();

	void create(const string& name, const Vector2& size, const Vector2& pos);

	void setTextName(const string& txt);

	void setBetValue(std::uint64_t value);
	void addBetValue(std::uint64_t value);

	std::uint64_t getBetValue();


private:
	void onEvent(Event* ev);

	spTextField _text_name;
	spTextField _text_bet;

	std::uint64_t _bet_value;
};

