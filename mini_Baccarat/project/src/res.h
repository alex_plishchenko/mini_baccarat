#pragma once
#include "oxygine-framework.h"
#include <unordered_map>

using namespace oxygine;

namespace TyGame
{
	extern Resources gameResources;
	
	void ResourcesLoad();
	void ResourcesFree();

	void playSound(const std::string& id);
}
